#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name='zonetool',
    version='0.1',
    description='Painless DNS zone management',
    author='ale',
    author_email='ale@incal.net',
    url='http://git.autistici.org/ale/zonetool',
    install_requires=['PyYAML'],
    test_requires=[],
    setup_requires=[],
    zip_safe=True,
    packages=find_packages(),
    package_data={},
    entry_points={
        'console_scripts': [
            'zonetool = zonetool.main:main',
            ],
        },
    )

